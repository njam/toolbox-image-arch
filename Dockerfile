FROM docker.io/library/archlinux

# The official image uses a pacman "NoExtract" rule to skip installation of
# manpages and non-EN locales. We revert this change by restoring an original
# pacman config, then reinstalling all packages.
# See https://gitlab.archlinux.org/archlinux/archlinux-docker/-/blob/301942f9e5995770cb5e4dedb4fe9166afa4806d/Makefile#L11
COPY config/pacman.conf /etc/pacman.conf
RUN pacman --noconfirm -Syu $(pacman -Qq)

# Install required packages
RUN pacman --needed --noconfirm -Syu \
  base \
  sudo \
  git \
  flatpak-xdg-utils `# To have 'flatpak-spawn' to execute commands on the host` \
  base-devel \
  systemd

# Install podman
RUN pacman --needed --noconfirm -Syu \
  podman \
  crun
COPY config/containers.conf /etc/containers/containers.conf
COPY config/containers-storage.conf /etc/containers/storage.conf

# Generate `/etc/machine-id`
RUN systemd-machine-id-setup

# Run customization setup
ADD setup.sh /tmp/setup.sh
RUN chmod a+x /tmp/setup.sh && \
    /tmp/setup.sh && \
    rm -f /tmp/setup.sh
