toolbox-image-arch
==================

My Arch Linux [toolbox](https://github.com/containers/toolbox) image.

Usage
-----

The image is built on Gitlab CI and available on
the [project's container registry](https://gitlab.com/njam/toolbox-image-arch/container_registry/) as:

```
registry.gitlab.com/njam/toolbox-image-arch
```

Development
-----------

Building the image manually:

```
podman build -t toolbox-image-arch .
```

Gitlab Runner for CI
--------------------

A custom [Gitlab Runner](https://docs.gitlab.com/runner/) is deployed to build the container image of this repo.
See [docs/gitlab-runner.md](docs/gitlab-runner.md) for details.
