Deploy a custom Gitlab Runner
=============================

A custom [Gitlab Runner](https://docs.gitlab.com/runner/install/) is deployed to build the container image of this repo,
because the default "shared runner" doesn't have enough disk space (22Gb of which 16Gb are available for the CI job).

-> [Instance on Hetzner](https://console.hetzner.cloud/projects/1269348/servers/51126876/overview) 

Maintenance
-----------

Restart the runner (aborts running builds):

```
sudo gitlab-runner restart
```

Check logs:

```
sudo journalctl -fu gitlab-runner
```

Setup
-----

### Prepare the server

1. Provision a Debian 10 VM (Debian 11 doesn't work well with gitlab-runner as of February 2022, probably due to cgroups
   v2 incompatibilities).
2. Provision a 50GB volume
3. Install docker: see https://docs.docker.com/engine/install/debian/
4. Install gitlab-runner: see https://docs.gitlab.com/runner/install/

### Configure gitlab-runner

1: Go to the project's [CI config](https://gitlab.com/njam/toolbox-image-arch/-/settings/ci_cd), click "New project runner" and configure:
- Tags: (none)
- Run untagged jobs: `[x]`
- Description: `runner for toolbox-image-arch`

2: On the next screen:
- Operating systems: `Linux`

3: In the terminal set the "token" from the gitlab registration page:
```sh
TOKEN=xxxxx
```

4: Configure the runner:
```sh
gitlab-runner register --non-interactive \
    --name "runner for toolbox-image-arch" \
    --executor "docker" \
    --url "https://gitlab.com/" \
    --token "${TOKEN}" \
    --docker-image "docker.io/library/alpine" \
    --docker-privileged
```

### Configure Docker `data-root`
Use an external volume to store Docker's data:
```
cat <<-'EOF' >/etc/docker/daemon.json
	{ 
	  "data-root": "/mnt/HC_Volume_101096108/docker-data" 
	}
EOF
```

Restart docker to apply:
```
systemctl restart docker
```

### Clear docker resources periodically
Create a systemd timer to prune docker resources:
```
cat <<-EOF >/etc/systemd/system/docker-prune.timer
[Unit]
Description=Prune docker resources every day

[Timer]
OnCalendar=daily
Persistent=true

[Install]
WantedBy=timers.target
EOF
```
```
cat <<-EOF >/etc/systemd/system/docker-prune.service
[Unit]
Description=Prune docker resources
Wants=docker-prune.timer

[Service]
Type=oneshot
ExecStart=/usr/bin/docker system prune --all --volumes -f
EOF
```

Enable the timer:
```
systemctl enable --now docker-prune.timer
```
