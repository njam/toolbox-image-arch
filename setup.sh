#!/usr/bin/env bash

set -euo pipefail

KEYMAP='us'
LOCALE_LANG='en_US.UTF-8'
LOCALE_NUM='de_CH.UTF-8'
TIMEZONE='Europe/Zurich'
COUNTRY='CH'
USER='user'

###########################################################################
### Add debug output for each command running in this script
###########################################################################

# See https://superuser.com/a/175802/280012
preexec_debug() {
  echo "$(date +'%T') RUN: ${BASH_COMMAND}"
}
trap 'preexec_debug' DEBUG


###########################################################################
### Helpers
###########################################################################
function search_replace() {
  FILE="${1}"
  SEARCH="${2}"
  REPLACE="${3}"
  sed -i "s/${SEARCH}/${REPLACE}/gw /tmp/changelog.txt" "${FILE}"
  if ! test -s /tmp/changelog.txt; then
    echo >&2 "Cannot find pattern '${SEARCH}' in file '${FILE}'."
    exit 1
  fi
}

###########################################################################
### System Basics
###########################################################################

# Time
rm -f /etc/localtime
ln -s /usr/share/zoneinfo/${TIMEZONE} /etc/localtime

# Keymap and Locale
echo "KEYMAP=${KEYMAP}" >/etc/vconsole.conf
cat <<-EOF >/etc/locale.gen
	${LOCALE_LANG} UTF-8
	${LOCALE_NUM} UTF-8
EOF
locale-gen
cat <<-EOF >/etc/locale.conf
	LANG=${LOCALE_LANG}
	LC_CTYPE=${LOCALE_NUM}
	LC_NUMERIC=${LOCALE_NUM}
	LC_TIME=${LOCALE_NUM}
	LC_COLLATE=${LOCALE_NUM}
	LC_MONETARY=${LOCALE_NUM}
	LC_MESSAGES=${LOCALE_LANG}
	LC_PAPER=${LOCALE_NUM}
	LC_NAME=${LOCALE_NUM}
	LC_ADDRESS=${LOCALE_NUM}
	LC_TELEPHONE=${LOCALE_NUM}
	LC_MEASUREMENT=${LOCALE_NUM}
	LC_IDENTIFICATION=${LOCALE_NUM}
EOF

# Pacman
MIRRORLIST="https://archlinux.org/mirrorlist/?country=${COUNTRY}&protocol=https&use_mirror_status=on"
curl -L --silent --show-error --fail "${MIRRORLIST}" | sed 's/^#Server/Server/' >/etc/pacman.d/mirrorlist
search_replace "/etc/pacman.conf" "^#Color$" "Color"
search_replace "/etc/pacman.conf" "^#VerbosePkgLists$" "VerbosePkgLists"
search_replace "/etc/makepkg.conf" "^COMPRESSXZ=.*" "COMPRESSXZ=(xz -c -z - --threads=0)"
search_replace "/etc/makepkg.conf" "^PKGEXT=.*" "PKGEXT='.pkg.tar.lzo'"
pacman -Syu --noconfirm
pacman -S --noconfirm --needed pkgfile lzop
pkgfile --update

# Yay
# Create a temporary user with sudo privileges without password,
# because 'yay' (and 'pacman') cannot run as root.
YAY_USER="yay"
useradd --system --create-home "yay"
echo "${YAY_USER} ALL=(ALL) NOPASSWD:ALL" >"/etc/sudoers.d/${YAY_USER}"
function yay_cleanup() {
  userdel --remove "${YAY_USER}"
  rm "/etc/sudoers.d/${YAY_USER}"
}
trap yay_cleanup EXIT
# Install yay
cd "$(mktemp -dt)"
curl -sL 'https://aur.archlinux.org/cgit/aur.git/snapshot/yay-bin.tar.gz' | tar --strip-components=1 -xzf -
chown -R "${YAY_USER}" .
sudo -u "${YAY_USER}" makepkg -s
pacman -U --noconfirm yay-*.pkg.tar.*
cd -
# Function to run yay
function yay_install() {
  sudo -u "${YAY_USER}" yay -S --noconfirm --needed "$@"
}

# Pacman cleanup-hook
yay_install pacman-cleanup-hook

# User namespace mapping
# Only if these files exist `useradd` will insert a mapping when creating a new user.
touch /etc/subuid /etc/subgid

###########################################################################
### Tools
###########################################################################

# Toolbelt
yay_install \
  sudo \
  git \
  curl \
  wget \
  base-devel \
  openssh \
  python-setuptools   `# Required to install 'python-pssh', see https://aur.archlinux.org/packages/python-pssh#comment-919868` \
  python-pssh \
  python-pipx   `# Required to install 'podman-compose'` \
  parallel \
  moreutils \
  tree \
  htop \
  iotop \
  iftop \
  inetutils   `# Includes 'telnet'` \
  man-db \
  perf \
  strace \
  lshw \
  oath-toolkit \
  rsync \
  `# TODO: installing 'lsyncd' started failing on CI with the update to '2.3.0' on 2022-07-11` \
  `#lsyncd` \
  bind    `# Includes 'dig'` \
  hdparm \
  wscat \
  whois \
  ripgrep \
  unp \
  openbsd-netcat

# Vim
yay_install vim vi-vim-symlink

# ZSH
yay_install zsh antigen

# MyRepos
yay_install myrepos

# Development tools
yay_install \
  git-town \
  heroku-cli-bin \
  nodejs yarn \
  python python-pip \
  terraform \
  aws-cli \
  google-cloud-cli \
  kubectl

# Development libraries/headers
yay_install \
  postgresql-libs

# Rust
yay_install rustup
rustup default stable
rustup target add x86_64-unknown-linux-musl
# Rust: cargo-binstall
curl -L --proto '=https' --tlsv1.2 -sSf https://raw.githubusercontent.com/cargo-bins/cargo-binstall/main/install-from-binstall-release.sh | bash
# Rust: cargo plugins
cargo binstall -y \
   cargo-make@0.35.8 \
   cargo-insta \
   flaky-finder
# cargo-release requires some special install options
cargo binstall -y --targets x86_64-unknown-linux-gnu \
   cargo-release@0.17.1
# Rust: copy installation toolchain into `/etc/skel` so it's available for any user on the system
mv /root/{.cargo,.rustup} /etc/skel/
# Rust: remove caches
rm -rf ~/.cargo/{git,registry}

# Vagrant
yay_install vagrant libvirt
# 2023-08-21 Disabled 'vagrant-libvirt', because installation failed with: "conflicting dependencies rexml (= 3.2.5) and rexml (= 3.2.6)"
#vagrant plugin install vagrant-libvirt

# ImageMagick
yay_install imagemagick ghostscript

# Podman
yay_install \
  podman \
  buildah \
  crun \
  fuse-overlayfs
cat <<-'EOF' >/usr/local/sbin/podman
	#!/bin/sh

	# Podman-in-podman only works as 'root' user, so we redirect calls of 'podman' to 'sudo podman'.
	# See https://github.com/containers/podman/issues/4056#issuecomment-673625989
	sudo /usr/sbin/podman "$@"
EOF
chmod a+x /usr/local/sbin/podman

# podman-compose
# The last stable release (1.0.3) is quite old and buggy, so we install a development release instead.
pipx install https://github.com/containers/podman-compose/archive/08ffcf6.tar.gz

###########################################################################
### Desktop Environment
###########################################################################

# Fonts
yay_install gnu-free-fonts ttf-liberation ttf-hack ttf-arphic-uming ttf-dejavu noto-fonts noto-fonts-emoji awesome-terminal-fonts-git
# TODO: these packages currently don't install due to a renamed dependency ("xorg-font-utils")
# - ttf-inconsolata-g

# IntelliJ
yay_install \
  intellij-idea-ultimate-edition \
  intellij-idea-ultimate-edition-jre \
  gtk3 `# Not strictly needed, but has some fonts and other features that are useful` \
  alsa-lib `# Required for '.md'-file preview rendering` \
  libxss `# Required for '.md'-file preview rendering`

# xdg-open: redirect to host
cat <<-'EOF' >/usr/local/sbin/xdg-open
	#!/bin/sh

	# Open URLs/files on the host
	flatpak-spawn --host xdg-open "$@"
EOF
chmod a+x /usr/local/sbin/xdg-open

###########################################################################
### Cleanup
###########################################################################

paccache -rk0
rm -rf /tmp/*
rm -rf ~/.cache/
